<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="buy1">

	<div class="line col-sm-12">
		<div class="line1 row">
			<P>${user.name}</P>
			<a href="logout" class="col-sm-5">ログアウト</a> <a href="cart"
				class="btn btn-primary">カート</a>

		</div>
	</div>
	<div class="col-3 mx-auto abc">

		<h1>購 入 画 面</h1>
	</div>
	<div class="col-4 mx-auto mt-5">

			<table>
				<th>商品名</th>
				<th>値段</th>
				<c:forEach var="list" items="${carlist}" varStatus="status">

					<tr height=100>
						<td width=700>${list.name}</td>
						<td width=250>${list.price}万円</td>
					</tr>
				</c:forEach>
			</table>
	</div>
		<form action="buynext" method="post">
	<div class="buy">
		<select name="deliveryid">
			<option value="">配送方法</option>
			<option value="1" >店頭</option>
			<option value="2">関東圏内</option>
			<option value="3" >その他の地域</option>
		</select>
	</div>
	<div class="col-3 mx-auto mt-5">

		<input type="submit" value="購入する" style="width: 250px; height: 50px;">
	</div>
	</form>


</body>
</html>

