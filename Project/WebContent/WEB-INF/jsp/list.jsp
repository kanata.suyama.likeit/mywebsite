<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="list3">

	<div class="line col-sm-12">
		<div class="line1 row">
			<P>${user.name}</P>
			<a href="logout" class="col-sm-5">ログアウト</a> <a href="cart"
				class="btn btn-primary">カート</a>

		</div>
	</div>
	<div class="aaa row">
		<div class="font">
			<h1>buy assets site</h1>
		</div>
		<c:if test="${user.loginid=='admin'}">
			<div class="ab">
				<a href="adminadd" class="btn btn-primary">追加</a>
			</div>
		</c:if>
	</div>
	<form action="search" method="post">
		<div class="list1 col-4 mx-auto row">
			<input type=text name=name placeholder="車の名前を入力してください"
				style="width: 350px; height: 50px"> <input type=submit
				value=検索>
		</div>
	</form>
	<div class="ab row">
		<c:forEach var="list" items="${list}" varStatus="status">
			<div class="list">
				<div class="card" style="width: 24rem;">
					<title>Placeholder</title> <img src="img/${list.file}" height="280px"
						width="380px">
					<div class="card-body">
						<h5 class="card-title">${list.name}</h5>
						<p class="card-text">${list.price}万円</p>
						<p class="card-text">${list.detail}</p>
						<a href="caradd?carid=${list.id}" class="btn btn-primary">カートに入れる</a>
					</div>
				</div>
				<div class="row">
					<c:if test="${user.loginid=='admin'}">
						<a href="admindelite?carid=${list.id}" class="btn btn-primary">削除</a>
						<a href="adminedit?carid=${list.id}" class="btn btn-primary">編集</a>
					</c:if>
				</div>
			</div>
			<c:if test="${(status.index+1) % 3 == 0 }">
	</div>
	<div class="ab row">
		</c:if>
		</c:forEach>
</body>
</html>
