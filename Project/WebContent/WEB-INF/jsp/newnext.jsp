
	<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="sea">
	<div class="col-sm-3 mx-auto">
		<h1>登録確認画面</h1>
	</div>
	<div class="col-sm-5 mx-auto">
		<h3>こちらの情報で登録してよろしいですか？</h3>
	</div>
	<div>

		<div class="aa col-4 mx-auto">
			<form action="usernext" method="post">
				<table>
					<tr height="80">
						<td class="abc" width=300>ユーザID</td>
						<td class="abc">${id}</td>
					</tr>
					<input type="hidden" name="id" value="${id}">

					<tr height="80">
						<td class="abc" width=300>パスワード</td>
						<td class="abc">${pass1}</td>
					</tr>
					<input type="hidden" name="pass" value="${pass1}">

					<tr height="80">
						<td class="abc" width=300>名前</td>
						<td class="abc">${name}</td>
					</tr>
					<input type="hidden" name="name" value="${name}">

					<tr height="80">
						<td class="abc" width=300>現住所</td>
						<td class="abc">${add}</td>
					</tr>
					<input type="hidden" name="add" value="${add}">

				</table>
		</div>
	</div>
	<div class="col-2 mx-auto" align="left">
		<input type="submit" value=登録>
	</div>
	</form>
</body>
</html>