<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="admin2">
	<div class="col-4 mx-auto">
		<h1 class="admin1">販売者情報編集画面</h1>
	</div>
	<form action="adminedit" method="post" enctype="multipart/form-data">

		<div class="aa col-4 mx-auto">

			<table>

				<input type="hidden" name="id" value="${list.id}">

				<tr height="80">
					<td width="350" align="center"><input type="text" name="name"
						value="${list.name }" style="width: 250px; height: 50px">
					</td>
				</tr>
				<tr height="80">
					<td width="350" align="center"><input type="text" name="price"
						value="${list.price }" style="width: 250px; height: 50px">
					</td>

				</tr>
				<tr height="80">
					<td width="350" align="center"><input type="text"
						name="detail" value="${list.detail}"
						style="width: 250px; height: 50px"></td>

				</tr>
				<tr height="80">
					<td width="350" align="center"><input type="file" name="file"
						value="${list.file}" style="width: 250px; height: 50px"></td>

				</tr>

			</table>
			<div class="list1 admin">
				<input type=submit value="変更する" style="width: 100px; height: 50px;">
			</div>
		</div>
	</form>
</body>
</html>