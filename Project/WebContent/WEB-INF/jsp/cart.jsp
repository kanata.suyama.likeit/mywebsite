<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="new3">

	<div class="line col-sm-12">
		<div class="line1 row">
			<P>${user.name}</P>
			<a href="logout" class="col-sm-5">ログアウト</a> <a href="cart"
				class="btn btn-primary">カート</a>

		</div>
	</div>
	<div class="col-3 mx-auto abc">
		<h1>カートの中身</h1>
	</div>
	<div class="ab row">
		<c:forEach var="list" items="${carlist}" varStatus="status">
			<div class="list">
				<div class="card" style="width: 22rem;">
					<title>Placeholder</title> <img src="img/${list.file}" height="280"
						width="350">
					<div class="card-body">
						<h5 class="card-title">${list.name}</h5>
						<p class="card-text">${list.price}万</p>

						<p class="card-text">${list.detail}</p>
					</div>
				</div>
				<a href="cardelete?carid=${list.id}" class="col-3 btn btn-primary">削除</a>
			</div>
			<c:if test="${(status.index+1) % 3 == 0 }">
	</div>
	<div class="ab row">
		</c:if>
		</c:forEach>

	</div>
	<c:if test="${not empty carlist}">
	<div class="col-4 mx-auto mt-5" align="center">
		<form action="buy" method="post">
			<input type=submit value="購入に進む" style="width: 250px; height: 50px">
			</c:if>
		</form>
	</div>

</body>

</html>
