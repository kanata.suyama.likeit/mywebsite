
	<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="admindelete">
    <div class="col-4 mx-auto">
        <h1>販売車削除最終確認画面</h1>
    </div>
    <div class="abc col-4 mx-auto" align="center">
        <p>
            <font size="5">${list.name}</font>
        </p>
        <p>
            <font size="5">${list.price}</font>
        </p>
        <p>
            <font size="5">${list.detail}</font>
        </p>
    </div>

    <div class="col-4 mx-auto">

        <div class="last row">
            <div class="col-sm-6">
                <form action="listServlet" method="post">
                    <input type="submit" value="一覧に戻る" style="width: 200px; height:50px;">
                </form>
            </div>
            <div class="col-sm-6">
                <form action="admindelite?id=${list.id}" method="post">
                    <input type="submit" value="削除" style="width: 200px; height:50px;">
                </form>
            </div>
        </div>
    </div>

</body></html>