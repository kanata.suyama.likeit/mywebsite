<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="CSS/list.css" rel="stylesheet" type="text/css" />
</head>

<body class="buy1">
    <div class="line col-sm-12">
        <div class="line1 row">
            <P></P>
            <a href="" class="col-sm-5">ログアウト</a>

            <a href="" class="btn btn-primary">カート</a>

        </div>
    </div>
    <div class="col-4 mx-auto abc" align="center">
        <h1>購入最終確認画面</h1>
    </div>
    <div class="col-4 mx-auto mt-5">


        <table>
        <c:forEach var="list" items="${carlist}" >
            <tr height=100>
                <td width=500>${list.name}</td>
                <td width=250>${list.price}万円</td>

            </tr>
           </c:forEach>
            <tr>

                <td>${delivery.name}</td>
                <td>${delivery.price}万円</td>

            </tr>
            <tr height=100>
                <td>合計金額</td>
                <td>${price}万円</td>
            </tr>

        </table>
    </div>
    <div class="col-3 mx-auto mt-5">
<form action="buylast" method="post">
        <input type="submit" value="購入を確定する" style="width: 250px; height:50px;">
        </form>
    </div>
    <div class="next">
        <a href="listServlet">戻る</a>
    </div>
</body></html>
