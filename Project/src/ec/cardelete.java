package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import user.carlist;

/**
 * Servlet implementation class delete
 */
@WebServlet("/cardelete")
public class cardelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public cardelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
try {
	int id = Integer.parseInt(request.getParameter("carid"));
	ArrayList<carlist> carlist = (ArrayList<carlist>) session.getAttribute("carlist");
	for(int i =carlist.size()-1;i>=0;i--){
		if(carlist.get(i).getId()==id) {
			carlist.remove(i);
			break;
		}

	}
	session.setAttribute("carlist", carlist);

	 response.sendRedirect("listServlet");

	}
	catch (Exception e) {
		e.printStackTrace();}


}





	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
