package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.deliverydao;
import dao.total;
import user.carlist;
import user.delivery;

/**
 * Servlet implementation class buy
 */
@WebServlet("/buynext")
public class buynext extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public buynext() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int deliveryid = Integer.parseInt(request.getParameter("deliveryid"));
		ArrayList<carlist> carlist = (ArrayList<carlist>) session.getAttribute("carlist");
		deliverydao dd=new deliverydao();
		delivery delivery=dd.delivery(deliveryid);
	    total t=new total();
	    int price=t.price(carlist);
	    int total=price+delivery.getPrice();

		request.setAttribute("price", total);
		request.setAttribute("delivery", delivery);
		session.setAttribute("carlist", carlist);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buynext.jsp");
		dispatcher.forward(request, response);


	}

}
