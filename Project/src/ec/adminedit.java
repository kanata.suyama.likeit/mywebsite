package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.admin;
import dao.listdao;
import user.carlist;

/**
 * Servlet implementation class admindetail
 */
@MultipartConfig(location="/tmp", maxFileSize=1048576)

@WebServlet("/adminedit")
public class adminedit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminedit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int id = Integer.parseInt(request.getParameter("carid"));
		listdao dao=new listdao();
		carlist list=dao.list(id);
		request.setAttribute("list",list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminedit.jsp");
		dispatcher.forward(request, response);

		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail= request.getParameter("detail");
		Part part = request.getPart("file");
        String  file = this.getFileName(part);
        part.write(getServletContext().getRealPath("img") + "/" + file);
		if (name.equals("") || detail.equals("") || file.equals("")|| price.equals("")) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminedit.jsp");
			dispatcher.forward(request, response);}
		int price1 = Integer.parseInt(price);

		admin dao =new admin();
		dao.up(id, name, price1, detail, file);
		 response.sendRedirect("listServlet");

	}
	private String getFileName(Part part) {
        String  file = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                file = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
               file = file.substring(file.lastIndexOf("\\") + 1);
                break;
            }
        }
        return file;
 }
}
