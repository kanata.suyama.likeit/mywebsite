package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DB.DB;
import user.list;

public class admin {

	public void add(String name, int price, String detail, String file) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DB.getConnection();
			String data = "insert into item_list(name,price,detail,file) VALUES (?,?,?,?)";
			ps = conn.prepareStatement(data);
			ps.setString(1, name);
			ps.setInt(2, price);
			ps.setString(3, detail);
			ps.setString(4, file);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

			System.out.println("入力された内容は正しくありません");

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void delete(int id) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DB.getConnection();
			String sql = "delete from item_list where id=?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<list> search(String name) {
		Connection conn = null;
		PreparedStatement ps = null;
		List<list> list = new ArrayList<list>();
		try {
			conn = DB.getConnection();
			String sql = "select * from item_list where name like ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + name + "%");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int itemid = rs.getInt("id");
				String name1 = rs.getString("name");
				int price = rs.getInt("price");
				String detail = rs.getString("detail");
				String file = rs.getString("file");
				list list2 = new list(itemid, name1, price, detail, file);
				list.add(list2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return list;

	}
	public void up(int id,String name,int price ,String detail,String file) {
		Connection conn = null;
		PreparedStatement ps =null;

		try {
			conn = DB.getConnection();

			String up="update item_list set name=?,price=?,detail=?,file=? where id=?";
			ps = conn.prepareStatement(up);
			ps.setString(1, name);
			ps.setInt(2, price);
			ps.setString(3, detail);
			ps.setString(4, file);
			ps.setInt(5,id);
			ps.executeUpdate();


		}
		catch (SQLException e) {
			e.printStackTrace();

		}
	}

}
