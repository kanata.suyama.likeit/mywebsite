
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DB.DB;
import user.user;

public class dao{
	public user login (String id ,String pass){
		Connection conn = null;
		try {
			conn=DB.getConnection();
			String data="select * from user where loginid=? and pass=?";
			PreparedStatement pstmt = conn.prepareStatement(data);
			pstmt.setString(1,id);
			pstmt.setString(2,pass);
			ResultSet result=pstmt.executeQuery();
			if(! result.next()) {
				return null;
			}
			int userid=result.getInt("id");
			String username=result.getString("name");
			String useradd=result.getString("address");
			String loginid=result.getString("loginid");
			user user=new user(userid,username,useradd,loginid);
			return user;




		}
		catch (SQLException e){
			e.printStackTrace();
			return null;
		}
	}
	public void user(String id,String name,String pass,String add) {
		Connection conn = null;
		PreparedStatement ps=null;
		try{
			conn=DB.getConnection();
			String data="insert into user(loginid,name,pass,address) VALUES (?,?,?,?)";
			ps = conn.prepareStatement(data);
			ps.setString(1,id);
			ps.setNString(2, name);
			ps.setString(3,pass);
			ps.setString(4, add);
			ps.executeUpdate();

		}
		catch(SQLException e){
			e.printStackTrace();

			System.out.println("入力された内容は正しくありません");

		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
	public user signup(String id) {
		Connection conn = null;
		try {
			conn = DB.getConnection();
			String sql = "SELECT * FROM user WHERE loginid = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;

			}
			int userid=rs.getInt("id");
			String username=rs.getString("name");
			String useradd=rs.getString("address");
			String loginid=rs.getString("liginid");
			user user=new user(userid,username,useradd,loginid);
			return user;
		}

		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}