package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DB.DB;
import user.delivery;

public class deliverydao {
	public static delivery delivery(int id) {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DB.getConnection();
			st=conn.prepareStatement("select * from delivery where id=?");
			st.setInt(1,id);
			ResultSet rs = st.executeQuery();
			delivery delivery=new delivery();
			if(rs.next()) {
				delivery.setId(rs.getInt("id"));
				delivery.setName(rs.getString("name"));
				delivery.setPrice(rs.getInt("price"));

			}
			return delivery;


		}
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}


	}
}
