package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DB.DB;
import user.carlist;
import user.list;

public class listdao {

	public List<list> findAll() {
		Connection conn = null;
		List<list> list = new ArrayList<list>();
		try {
			conn = DB.getConnection();
			String data = "select * from item_list";
			Statement stmt = conn.createStatement();
			ResultSet ab = stmt.executeQuery(data);
			while (ab.next()) {
				int itemid = ab.getInt("id");
				String name = ab.getString("name");
				int price = ab.getInt("price");
				String detail = ab.getString("detail");
				String file = ab.getString("file");
				list list2 = new list(itemid, name, price, detail, file);
				list.add(list2);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return list;
	}
	public static carlist list(int id) {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DB.getConnection();
			st=conn.prepareStatement("select * from item_list where id=?");
			st.setInt(1,id);
			ResultSet rs = st.executeQuery();
			carlist car=new carlist();
			if(rs.next()) {
				car.setId(rs.getInt("id"));
				car.setName(rs.getString("name"));
				car.setPrice(rs.getInt("price"));
				car.setDetail(rs.getString("detail"));
				car.setFile(rs.getString("file"));
			}
			return car;


		}
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}


	}
}
