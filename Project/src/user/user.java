
package user;

import java.io.Serializable;

public class user  implements Serializable {
	private int id;
	private  String loginid;
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	private  String name;
	private String pass;
	private String address;

	public user(int id,String name,String address,String loginid) {
		this.id=id;
		this.name=name;
		this.address=address;
		this.loginid = loginid;

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}