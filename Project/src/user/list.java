package user;

import java.io.Serializable;

public class list implements Serializable {
private int id;
private String name;
private int price;
private String detail;
private String file;

public list(int id,String name,int price,String detail,String file) {
	this.id=id;
	this.name=name;
	this.price=price;
	this.detail=detail;
	this.file=file;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getDetail() {
	return detail;
}
public void setDetail(String detail) {
	this.detail = detail;
}
public String getFile() {
	return file;
}
public void setFile(String file) {
	this.file = file;
}


}
